import React, {Component} from 'react';

class Body extends Component{

    constructor(props) {
        super(props);
        this.state = {
            name:"Anthony",
        };
    }


    render() {
        return(
            
            <div>
                <p>Hola!</p>
                <p> { this.state.name } </p>

            </div>
        );
    }

}

export default Body;