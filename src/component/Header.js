import React from 'react';
import Body from './Body';

const Header = () => {

    return(
        <div>
            <h1>SNOOP CONSULTING</h1>
            <Body/>
        </div>
    );
}

export default Header;